// pages/everydayexe/everydayexe.js
const app = getApp();
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    dayList:null
  },
//录入试题
entryExamTap:function(){
  wx.navigateTo({
    url: '../addSubject/addSubject'
  })
},
entryExeTap:function(res){
  if(util.isUserLogin()){
    wx.navigateTo({
      url: '../login/login',
    })
    return
  }
  var exerdate=  res.currentTarget.dataset.exerdate
  wx.navigateTo({
    url: '../exam/exam?exerdate=' +exerdate+'&searchType=everyday'
  })
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var url = "exercise/dayList"
    // console.log(url)
    this.setData({
      userInfo: wx.getStorageSync("userInfo") || "",
      // showTime:this.showTime()
    })
    console.log(this.data.userInfo)
    util.getHttp(url, this.getEverydayExeList_callBack)
  },

  getEverydayExeList_callBack: function (res) {
    console.log(res.data)
    this.setData({
      dayList:res.data
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      userInfo: wx.getStorageSync("userInfo") || "",
      // showTime:this.showTime()
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})