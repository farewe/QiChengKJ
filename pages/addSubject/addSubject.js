const app = getApp();
const util = require('../../utils/util.js')
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    //章节
    allChapters:null,
    chapterIndex: null,
    chapters: [],
    //科目
    subjectValue:"",
    subjectIndex: null,
    subjectTypes: [
      ['初级会计实务', 'prac'],
      ['初级经济基础', 'econ']
    ],

    selectOptionType: 'radio', // 题目类型
    optionType: [{
        type: 'radio',
        name: '单选题',
        checked: 'true'
      },
      {
        type: 'multiple',
        name: '多选题'
      },
      {
        type: 'judge',
        name: '判断题'
      }
    ],
    questionContext: {
      question: '',
      optionA: '',
      optionB: '',
      optionC: '',
      optionD: '',
      analysis: '',
    },
    ischeckbox:false,//复选按钮复位
    answer: '',
    multiArray: [
      ['无脊柱动物', '脊柱动物'],
      ['扁性动物', '线形动物', '环节动物', '软体动物', '节肢动物'],
      ['猪肉绦虫', '吸血虫']
    ],
    objectMultiArray: [
      [{
          id: 0,
          name: '无脊柱动物'
        },
        {
          id: 1,
          name: '脊柱动物'
        }
      ],
      [{
          id: 0,
          name: '扁性动物'
        },
        {
          id: 1,
          name: '线形动物'
        },
        {
          id: 2,
          name: '环节动物'
        },
        {
          id: 3,
          name: '软体动物'
        },
        {
          id: 3,
          name: '节肢动物'
        }
      ],
      [{
          id: 0,
          name: '猪肉绦虫'
        },
        {
          id: 1,
          name: '吸血虫'
        }
      ]
    ],
    multiIndex: [0, 0, 0],
    time: '12:01',
    currentTime: '',
    region: ['广东省', '广州市', '海珠区'],
    imgList: [],
    modalName: null,
  },
  //科目或者章节
  PickerChange(res) {
    //获取操作类型和选择序号
    var optionType = res.currentTarget.dataset.optionType;
    var index = res.detail.value
    if (optionType == "subject") {
      //获取章节
      var subjectValue = this.data.subjectTypes[index][1]
      var chapters = this.data.allChapters[subjectValue]
      this.setData({
        subjectIndex: index,
        chapters:chapters
      })
    } else if(optionType == "chapter") {
      this.setData({
        chapterIndex: index
      })
    }
  },

  MultiChange(e) {
    this.setData({
      multiIndex: e.detail.value
    })
  },
  MultiColumnChange(e) {
    let data = {
      multiArray: this.data.multiArray,
      multiIndex: this.data.multiIndex
    };
    data.multiIndex[e.detail.column] = e.detail.value;
    switch (e.detail.column) {
      case 0:
        switch (data.multiIndex[0]) {
          case 0:
            data.multiArray[1] = ['扁性动物', '线形动物', '环节动物', '软体动物', '节肢动物'];
            data.multiArray[2] = ['猪肉绦虫', '吸血虫'];
            break;
          case 1:
            data.multiArray[1] = ['鱼', '两栖动物', '爬行动物'];
            data.multiArray[2] = ['鲫鱼', '带鱼'];
            break;
        }
        data.multiIndex[1] = 0;
        data.multiIndex[2] = 0;
        break;
      case 1:
        switch (data.multiIndex[0]) {
          case 0:
            switch (data.multiIndex[1]) {
              case 0:
                data.multiArray[2] = ['猪肉绦虫', '吸血虫'];
                break;
              case 1:
                data.multiArray[2] = ['蛔虫'];
                break;
              case 2:
                data.multiArray[2] = ['蚂蚁', '蚂蟥'];
                break;
              case 3:
                data.multiArray[2] = ['河蚌', '蜗牛', '蛞蝓'];
                break;
              case 4:
                data.multiArray[2] = ['昆虫', '甲壳动物', '蛛形动物', '多足动物'];
                break;
            }
            break;
          case 1:
            switch (data.multiIndex[1]) {
              case 0:
                data.multiArray[2] = ['鲫鱼', '带鱼'];
                break;
              case 1:
                data.multiArray[2] = ['青蛙', '娃娃鱼'];
                break;
              case 2:
                data.multiArray[2] = ['蜥蜴', '龟', '壁虎'];
                break;
            }
            break;
        }
        data.multiIndex[2] = 0;
        break;
    }
    this.setData(data);
  },
  TimeChange(e) {
    this.setData({
      time: e.detail.value
    })
  },
  DateChange(e) {
    this.setData({
      date: e.detail.value
    })
  },
  RegionChange: function (e) {
    this.setData({
      region: e.detail.value
    })
  },
  ChooseImage() {
    wx.chooseImage({
      count: 4, //默认9
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], //从相册选择
      success: (res) => {
        if (this.data.imgList.length != 0) {
          this.setData({
            imgList: this.data.imgList.concat(res.tempFilePaths)
          })
        } else {
          this.setData({
            imgList: res.tempFilePaths
          })
        }
      }
    });
  },
  ViewImage(e) {
    wx.previewImage({
      urls: this.data.imgList,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg(e) {
    wx.showModal({
      title: '召唤师',
      content: '确定要删除这段回忆吗？',
      cancelText: '再看看',
      confirmText: '再见',
      success: res => {
        if (res.confirm) {
          this.data.imgList.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList: this.data.imgList
          })
        }
      }
    })
  },
  //题目类型
  optionTypeTap: function (res) {
    // 如果是判断题
    var questionContext =  {}
    if(res.detail.value == "judge"){
      questionContext =  {
        optionA: '对',
        optionB: '错',
      }
    }
    this.setData({
      selectOptionType: res.detail.value,
      questionContext:questionContext
    })
  },
  //input输入内容
  getInputInfoTap: function (res) {
    var inputValue = res.detail.value
    if (inputValue == '') {
      return;
    }
    var questionContext = this.data.questionContext
    var IntpuType = res.currentTarget.dataset.inputType;
    questionContext[IntpuType] = inputValue
    this.setData({
      questionContext: questionContext
    })
  },
  //答案
  checkboxAnswerTap: function (res) {
   this.setData({
    answer:res.detail.value
   })
  },
  submitTap: function () {
    //章节
    if (!util.checkInfo(this.data.subjectIndex, "未选择科目") ||
        !util.checkInfo(this.data.chapterIndex,"未选择章节")||
        !util.checkInfo(this.data.questionContext.question,"题目为空")||
        !util.checkInfo(this.data.answer,"答案为空")) {
      return;
    }
    //判断题可以为空
    if(this.data.selectOptionType != "judge"){
        if(!util.checkInfo(this.data.questionContext.optionA,"选项A为空") ||
          !util.checkInfo(this.data.questionContext.optionB,"选项B为空") ||
          !util.checkInfo(this.data.questionContext.optionC,"选项C为空") ||
          !util.checkInfo(this.data.questionContext.optionD,"选项D为空") ){
          return;
        }
    }
    var appDate = this.data
    var data = {
      subjectType:appDate.subjectTypes[appDate.subjectIndex][1],
      question:appDate.questionContext.question,
      optionA:appDate.questionContext.optionA,
      optionB:appDate.questionContext.optionB,
      optionC:appDate.questionContext.optionC,
      optionD:appDate.questionContext.optionD,
      rightAnswer:this.formatAnswer(appDate.answer),
      analysis:appDate.questionContext.analysis,
      chapterId:appDate.chapters[appDate.chapterIndex][1],
      exerDate: this.data.currentTime, //时间
      optionType: this.data.selectOptionType, //题目类型
    }
    //提交
    var url = "exercise/record";
    util.postHttp(url, data, this.submitExercise_callBack)
  },
  submitExercise_callBack:function(res){
    console.log(res)
    wx.showModal({
      title: '提示',
      content: "提交成功",
      showCancel: false,
      success(res) {
        if (res.confirm) {
          return false
        }
      }
    })
  },
  formatAnswer:function(formatData){
    var data = formatData[0];
      for(var i = 1;i<formatData.length;i++){
          if(i != formatData.length){
              data+=','
          }
          data+=formatData[i]
      }
      return data
  },
  //清空
  cleanTap:function(){
      var questionContext =  {
        question: '',
        optionA: '',
        optionB: '',
        optionC: '',
        optionD: '',
        analysis: '',
      }
      this.setData({
        questionContext:questionContext,
        ischeckbox:false
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     //获取当前时间
     var currentTime = util.formatTimeYYMMDD(new Date());
     this.setData({
      currentTime:currentTime
    })
    this.formatAnswer(['c','a'])
    //获取所有章节
    var url = "chapter/all"
    util.getHttp(url, this.getAllChapters_callBack)
  },
  getAllChapters_callBack: function (res) {
    this.setData({
      allChapters:res.data
    })
  },
})