// pages/collect/collect.js
const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabNav: [{
      value: 'econ',
      name: '初级经济法基础'
    }, {
      value: 'prac',
      name: '初级会计实务'
    }, {
      value: 'noValue',
      name: '会计基础'
    }, {
      value: 'noValue',
      name: '中级会计实务'
    }, {
      value: 'noValue',
      name: '中级经济法基础'
    }, {
      value: 'noValue',
      name: '财务管理'
    }, {
      value: 'noValue',
      name: '直播公开课'
    }],
    subjectType:'econ'
  },
  //导航栏
  tabSelect(e) {
    if(e.currentTarget.dataset.value == 'noValue'){
      wx.showModal({
        title: '提示',
        content: "您未开通此课程!",
        showCancel: false
      })
      return;
    }
    this.setData({
      subjectType: e.currentTarget.dataset.value
    })
    this.getChapterList(this.data.searchType)

  },
  //开始练习
  examTap:function(e){
    var chapterid = e.currentTarget.dataset.chapterid
    wx.navigateTo({
      url: '../exam/exam?chapterId=' + chapterid + "&subjectType=" + this.data.subjectType + "&searchType="+this.data.searchType,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.searchType == ""){

    }
    this.setData({
      userInfo: wx.getStorageSync("userInfo") || "",
      searchType:options.searchType
    })
    console.log(options.searchType)
    this.getChapterList(options.searchType)

  },
  //章节列表
  getChapterList:function(searchType){
    var url = "chapter/list?userId=" + this.data.userInfo["userId"] + "&subjectType=" + this.data.subjectType +"&searchType="+searchType
    util.getHttp(url, this.getChapterList_callBack)
  },
  getChapterList_callBack: function (res) {
    console.log(res)
    this.setData({
      chapterList: res.data
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})