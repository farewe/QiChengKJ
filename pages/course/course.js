// pages/detail/detail.js
const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    setInter: '', //定时器
    CustomBar: app.globalData.CustomBar,
    TabCur: 0,
    tabNav: [{
      value: 'econ',
      name: '初级经济法基础'
    }, {
      value: 'prac',
      name: '初级会计实务'
    }, {
      value: 'noValue',
      name: '会计基础'
    }, {
      value: 'noValue',
      name: '中级会计实务'
    }, {
      value: 'noValue',
      name: '中级经济法基础'
    }, {
      value: 'noValue',
      name: '财务管理'
    }, {
      value: 'noValue',
      name: '直播公开课'
    }],
    swiperList: [{
      id: 0,
      type: 'image',
      url: 'https://mobile.qichengkuaiji.com:8333/qicheng/image/banner8.jpg'
    }, {
      id: 1,
      type: 'image',
      url: 'https://mobile.qichengkuaiji.com:8333/qicheng/image/banner9.jpg',
    }, {
      id: 2,
      type: 'image',
      url: 'https://mobile.qichengkuaiji.com:8333/qicheng/image/banner10.jpg'
    }, {
      id: 3,
      type: 'image',
      url: 'https://mobile.qichengkuaiji.com:8333/qicheng/image/banner6.jpg'
    }, {
      id: 4,
      type: 'image',
      url: 'https://mobile.qichengkuaiji.com:8333/qicheng/image/banner7.jpg'
    }],
    iconList: [{
        icon: './../../image/editFile.png',
        color: 'red',
        badge: 120,
        option: 'everyDayTest',
        name: '每日一练'
      },
      {
        icon: './../../image/chaptallist.png',
        color: 'orange',
        badge: 1,
        option: 'SubjectTest',
        showNodel: 1,
        name: '章节练习'
      },
      {
        icon: './../../image/errorList.png',
        color: 'yellow',
        badge: 0,
        option: 'errorText',
        name: '错题本'
      }, {
        icon: './../../image/testClock.png',
        color: 'yellow',
        badge: 0,
        option: 'simulationTest',
        name: '模拟考试'
      }
    ],
    userInfo: null,
    subjectType: "econ",
    chapterList: []
  },
  //滑动
  tabSelect(e) {
    
    if(e.currentTarget.dataset.value == "noValue"){
      wx.showModal({
        title: '提示',
        content: "您未开通此课程!",
        showCancel: false
      })
      return;
    }
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      scrollLeft: (e.currentTarget.dataset.id - 1) * 60,
      subjectType:e.currentTarget.dataset.value
    })
    this.getChapterList()

   
  },
  //四选一
  optionTap: function (res) {
    console.log(res.currentTarget.dataset.optionType)
    //每日一练
    if (res.currentTarget.dataset.optionType == "everyDayTest") {
      console.log("每日一练");
      wx.navigateTo({
        url: '../everydayexe/everydayexe'
      })
    }
    //章节练习
    else if (res.currentTarget.dataset.optionType == "SubjectTest") {
      // this.showModal("RadioModal")
      // wx.showModal({
      //   title: '提示',
      //   content: "您未开通开通此课程!",
      //   showCancel: false
      // })
    }
    //错题本
    else if (res.currentTarget.dataset.optionType == "errorText") {
      // console.log("错题本")
      wx.navigateTo({
        url: '../collect/collect?searchType=error'
      })
    }
    //模拟考试
    else if (res.currentTarget.dataset.optionType == "simulationTest") {
      // console.log("模拟考试")
      wx.showModal({
        title: '提示',
        content: "您未开通此课程!",
        showCancel: false
      })
    }
  },

  //章节练习
  showModal(modalName) {
    this.setData({
      modalName: modalName
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
   numberFormat:function(value) {
    var v = parseInt(value)//强转Int，毕竟有可能返回是String类型的数字
    return v.toFixed(2)
  },
  /**
   *  跳转题目列表
   */
  startChapterTest: function (res) {
    if(util.isUserLogin()){
      wx.navigateTo({
        url: '../login/login',
      })
      return
    }
    var chapterId = res.currentTarget.dataset.chapterId

    wx.navigateTo({
      url: '../exam/exam?chapterId=' + chapterId + "&subjectType=" + this.data.subjectType + "&searchType=default",
    })

  },
  showTime: function () {
    var nowtime = new Date(), //获取当前时间
      endtime = new Date("2021/5/12"); //定义结束时间
    var lefttime = endtime.getTime() - nowtime.getTime(), //距离结束时间的毫秒数
      leftd = Math.floor(lefttime / (1000 * 60 * 60 * 24)), //计算天数
      lefth = Math.floor(lefttime / (1000 * 60 * 60) % 24), //计算小时数 
      leftm = Math.floor(lefttime / (1000 * 60) % 60), //计算分钟数
      lefts = Math.floor(lefttime / 1000 % 60); //计算秒数
    return leftd + "天" + lefth + ":" + leftm + ":" + lefts; //返回倒计时的字符串
  },
  startSetInter: function () {
    var that = this;
    //将计时器赋值给setInter
    that.data.setInter = setInterval(
      function () {
        that.setData({
          countdownTime: that.showTime()
        });
      }, 1000);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userInfo: wx.getStorageSync("userInfo") || "",
      // showTime:this.showTime()
    })
    this.startSetInter()
    this.getChapterList()
  },
  getChapterList:function(){
    var url = "chapter/list?userId=" + this.data.userInfo["userId"] + "&subjectType=" + this.data.subjectType
    util.getHttp(url, this.getChapterList_callBack)
  },

  getChapterList_callBack: function (res) {
    console.log(res)
    this.setData({
      chapterList: res.data
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      userInfo: wx.getStorageSync("userInfo") || "",
    })
    this.startSetInter()
    this.getChapterList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    var that = this;
    //清除计时器  即清除setInter
    clearInterval(that.data.setInter)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

})
// 倒计时
function countdown(that) {
  var EndTime = new Date("2020-04-06").getTime() || [];
  // console.log(EndTime);
  var NowTime = new Date().getTime();
  var total_micro_second = EndTime - NowTime || []; //单位毫秒
  if (total_micro_second < 0) {
    // console.log('时间初始化小于0，活动已结束状态');
    total_micro_second = 1; //单位毫秒 ------ WHY？
  }
  // console.log('剩余时间：' + total_micro_second);
  // 渲染倒计时时钟
  that.setData({
    clock: dateformat(total_micro_second) //若已结束，此处输出'0天0小时0分钟0秒'
  });
  if (total_micro_second <= 0) {
    that.setData({
      clock: "已经截止"
    });
    return;
  }
  setTimeout(function () {
    total_micro_second -= 1000;
    countdown(that);
  }, 1000)
}

// 时间格式化输出，如11天03小时25分钟19秒 每1s都会调用一次
function dateformat(micro_second) {
  // 总秒数
  var second = Math.floor(micro_second / 1000);
  // 天数
  var day = Math.floor(second / 3600 / 24);
  // 小时
  var hr = Math.floor(second / 3600 % 24);
  // 分钟
  var min = Math.floor(second / 60 % 60);
  // 秒
  var sec = Math.floor(second % 60);
  return day + "天" + hr + "小时" + min + "分钟" + sec + "秒";
}