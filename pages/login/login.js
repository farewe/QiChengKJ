// pages/login/login.js
const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
//获取账户
getAccount: function (res) {

  this.setData({
    account: res.detail.value
  })
  console.log(this.data.account)
},
//获取密码
getPassword: function (res) {

  this.setData({
    password: res.detail.value
  })
  console.log(this.data.password)
},
loginBtn: function () {

  //验证不为空
  var account_is_null = util.checkInfo(this.data.account, "手机号不能为空")

  if (account_is_null) {
    var password_is_null = util.checkInfo(this.data.password, "密码不能为空")
  }

  if (account_is_null && password_is_null) {
    var url = "user/login?account=" + this.data.account + "&password=" + this.data.password
    util.getHttp(url, this.login_callBack)
  }

},
login_callBack: function (res) {

  if (res.data) {
    console.log(res.data)
    wx.setStorageSync('userInfo',res.data)
    wx.switchTab({
      url: '../course/course'
    })
  } else {
    var password_is_null = util.checkInfo("", "登录失败,账号或密码不正确!")
  }
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var userInfo = wx.getStorageSync("userInfo") || "";
    this.setData({
      wxUserInfo: app.globalData.userInfo,
      userInfo: userInfo,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})