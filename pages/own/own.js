// pages/own/own.js

const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    wxUserInfo: null,
    userInfo: null,
    isShowModal: false,
    motto: '',
    manifesto: ''

  },
  /**退出登录 */
  logoutTap:function(){
    wx.showModal({
      title: '提示',
      content: "你确定退出登录吗？", 
      showCancel: false,
      success (res) {
        wx.setStorageSync('userInfo',"")
      }
    })
  },
  /**
   * 我的收藏
   */
  mycollectTap:function(){
    wx.navigateTo({
      url: '../collect/collect?searchType=collection'
    })
  },
  /**
   * 我的宣言 
   */
  //显示
  showModelTap: function (res) {
    if(util.isUserLogin()){
      wx.navigateTo({
        url: '../login/login',
      })
      return
    }
    const that = this;
    this.setData({
      isShowModal: !that.data.isShowModal
    });
    console.log(res);
    if (res.currentTarget.dataset.operateType == "sure") {
      var url = "user/manifesto?userId=" + this.data.userInfo.userId + "&manifesto=" + this.data.manifesto;
      util.getHttp(url, this.setMoto_callBack)
    }
  },
  setMoto_callBack: function (res) {
    //修改本地保存的用户数据
    var userInfo = this.data.userInfo;
    userInfo.manifesto = this.data.manifesto
    wx.setStorageSync('userInfo', userInfo)
    this.setData({
      motto:userInfo.manifesto
    })
  },
  //获取输入的宣言
  getMotto: function (res) {
    this.setData({
      manifesto: res.detail.value
    })
  },
  loginTap:function(){
    wx:wx.navigateTo({
      url: '../login/login'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var userInfo = wx.getStorageSync("userInfo") || "";
    console.log(userInfo)
    console.log(app.globalData.userInfo)
    this.setData({
      wxUserInfo: app.globalData.userInfo,
      userInfo: userInfo,
      motto: userInfo.manifesto || ""
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var userInfo = wx.getStorageSync("userInfo") || "";
    console.log(app.globalData.userInfo)
    this.setData({
      userInfo: userInfo,
      motto: userInfo.manifesto || ""
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})