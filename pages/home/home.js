// pages/home/home.js


Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperList: [{
        id: 0,
        type: 'image',
        url: 'https://mobile.qichengkuaiji.com:8333/qicheng/image/banner1.jpg'
      }, {
        id: 1,
        type: 'image',
        url: 'https://mobile.qichengkuaiji.com:8333/qicheng/image/banner2.jpg',
      }, {
        id: 2,
        type: 'image',
        url: 'https://mobile.qichengkuaiji.com:8333/qicheng/image/banner3.jpg'
      }, {
        id: 3,
        type: 'image',
        url: 'https://mobile.qichengkuaiji.com:8333/qicheng/image/banner4.jpg'
      },
      {
        id: 4,
        type: 'image',
        url: 'https://mobile.qichengkuaiji.com:8333/qicheng/image/banner5.jpg'
      }
    ],

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.towerSwiper("swiperList")
    console.log(this.data.swiperList)
  },
  //初始化towerSwiper
  towerSwiper: function (name) {
    let list = this.data[name];
    for (let i = 0; i < list.length; i++) {
      list[i].zIndex = parseInt(list.length / 2) + 1 - Math.abs(i - parseInt(list.length / 2))
      list[i].mLeft = i - parseInt(list.length / 2)
    }
    this.setData({
      swiperList: list
    })
  },
  DotStyle(e) {
    this.setData({

      DotStyle: e.detail.value
    })
  },

  /**
   * 跳转文章详情
   */
  toArticleDetail: function () {
    wx.navigateTo({
      url: '../detail/detail'
    })
  },
  toArticleDetaila: function () {
    wx.navigateTo({
      url: '../detaila/detaila'
    })
  },
  toArticleDetailb: function () {
    wx.navigateTo({
      url: '../detailb/detailb'
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {


  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})