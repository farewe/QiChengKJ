class LocalData {
  constructor() {
    this.storageKeyName = 'postList';
    
  }
 
  /**得到图片的选择*/
  getOptionsNum(conditions){
    var optionsNum = [0, 0, 0, 0]
    switch (conditions) {
      case "a":
        optionsNum[0] = 1
        break;
      case "b":
        optionsNum[1] = 1
        break;
      case "c":
        optionsNum[2] = 1
        break;
      case "d":
        optionsNum[3] = 1
        break;
    }
    return optionsNum
  }
  // 直接初始化全部问题选择[1:做了，0：未做]   [1:正确，0：错误]
  initSelectAnswer(questions){
    var selectanswer={}
    for (var i=0;i<questions.length;i++){
      selectanswer[questions[i]['id']] = ['', 0]
    }
    return selectanswer
  }
  /**得到用户的选择 */
  getSelectAnswer(selectanswer, question_id,selected){
    selectanswer[question_id] = ['', 1]
    switch (selected) {
      case "a":
        selectanswer[question_id][0] = 'a'
        break;
      case "b":
        selectanswer[question_id][0] = 'b'
        break;
      case "c":
        selectanswer[question_id][0] = 'c'
        break;
      case "d":
        selectanswer[question_id][0] = 'd'
        break;
    }
    return selectanswer
  }
  
  /*初始化缓存数据
    storageKeyName:缓存的键
    data:值
  */
  
  execSetStorageSync(storageKeyName,data) {
    wx.setStorageSync(storageKeyName, data);
  }
/**得到缓存信息 */
  getSetStorageSync(storageKeyName) {
    return wx.getStorageSync(storageKeyName)
  }
  
  /**
   * 
   * 1.selectAnswer()
   {21: "b", 22: "b", 23: "b", 24: "b", 25: "b", 26: "b", 27: "b"}
   * 
   selectAnswer:{21:['b',]}
   * 
   * 
   * 2.optionsNum = [0, 0, 0, 0]
   * 
   * 
   */

};
export { LocalData}