const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}
const formatTimeYYMMDD = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  
  return [year, month, day].map(formatNumber).join('-')
}
const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}



const app = getApp()

//get请求
function getHttp(url,callBack){
  console.log(app.globalData.qichengurl+url)
  wx.request({
    url: app.globalData.qichengurl+url,
    method:'GET',
    header:{
      "Content-Type": "application/json"
    },
    success:function(res){
      if(res.statusCode == 200){
        callBack(res);
      }
      
    },
    fail:function(err){
      console.log('fail')
      console.log(err)
    }
  })
}
//post请求
function postHttp(url,data,callBack){
  console.log(app.globalData.qichengurl+url)
  console.log(data)
  wx.request({
    url: app.globalData.qichengurl+url,
    method:'POST',
    data:data,
    header:{
      "Content-Type": " application/x-www-form-urlencoded"
    },
    success:function(res){
      callBack(res);
    },
    fail:function(err){
      console.log(err)
    }
  })
}

//字段验证
function checkInfo(field,msg) {
  if (!field) {
    wx.showModal({
      title: '提示',
      content: msg,
      showCancel: false,
      success(res) {
        if (res.confirm) {
          return false
        }
      }
    })

  } else {
    return true
  }
}

function isUserLogin(){

  var userInfo = wx.getStorageSync("userInfo") || ""
  console.log("------------1---")
  if(userInfo == ""){
    console.log("----------2-----")
    return true
  }
  return false
  console.log("--------3-------")
}

module.exports = {
  formatTime: formatTime,
  formatTimeYYMMDD:formatTimeYYMMDD,
  getHttp:getHttp,
  checkInfo:checkInfo,
  postHttp:postHttp,
  isUserLogin:isUserLogin
}
